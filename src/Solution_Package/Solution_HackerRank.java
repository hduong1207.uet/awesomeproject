package Solution_Package;
import java.util.*;

public class Solution_HackerRank {
       public static void main(String arg[]){
           Scanner scanner_x = new Scanner(System.in);
           System.out.println("-->Chuyển chuỗi từ bàn phím sang ký tự ' viết thường '...");
           System.out.println("Nhập số chuỗi muốn chuyển :");
           //Khoi tao bien so luong  chuoi
           int n = scanner_x.nextInt();
           //Nhap cac chuoi tu ban phim - chuyen - in ra man hinh
           for(int i=0 ; i < n ; i++){
               String str = scanner_x.next();
               System.out.println("-->" + str.toLowerCase());
           }//end for
           scanner_x.close();
       }// end main

}
